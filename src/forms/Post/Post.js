import React, {  useEffect, useRef, useState } from "react";

function Post(props){
  const { openForm, handleSubmitForm } = props;
  const [showLocationBtn, setShowLocationBtn] = useState(true);
  const [buttonLoading, setButtonLoading] = useState(false);
  const [location, setLocation] = useState({ lat: 0, lng: 0});
  const refLocationInput = useRef(null);
  const refCaptionInput = useRef(null);
  const refUsernameInput = useRef(null);

  useEffect(() => {
    if(openForm){
      initLocation();
    }
  }, [openForm]);

  const initLocation = () => {
    if(!('geolocation' in navigator)) {
      setShowLocationBtn(false);
    }
  }

  const getLocation = () => {
    if(!('geolocation' in navigator)) {
      return;
    }

    setButtonLoading(true);
    navigator.geolocation.getCurrentPosition(function(position) {
      console.log('position :>> ', position);
      setButtonLoading(false);
      setLocation({ lat: position.coords.latitude, lng: position.coords.longitude });
      refLocationInput.current.value = 'Jombang';
      refLocationInput.current.classList.add('focus:border-blue-300');
    }, function(err){
      console.log('err location :>> ', err);
      setButtonLoading(false)
      alert('Couldnt find location, input manually please');
      setLocation({ lat: 0, lng: 0 });
    }, { timeout: 10000 })
  }

  const handleSubmit = e => {
    e.preventDefault();
    const values = {
      username: refUsernameInput.current.value,
      caption: refCaptionInput.current.value,
      location: refLocationInput.current.value,
      rawLocation: location
    };
    handleSubmitForm(values);
  }

  return(
    <form className="text-left" onSubmit={handleSubmit}>
      <div className="mb-5">
        <label htmlFor="username" className="text-sm font-medium text-gray-900 block mb-2">Nama</label>
        <input type="text" id="username" ref={refUsernameInput} autoComplete={false}
          className="bg-gray-50 border border-gray-300 text-gray-900 mb-3 sm:text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
          required></input>
      </div>
      <div className="mb-5">
        <label htmlFor="caption" className="text-sm font-medium text-gray-900 block mb-2">Note</label>
        <textarea ref={refCaptionInput} type="text" id="caption"
          className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
          required rows="5"></textarea>
      </div>
      <div className="mb-10">
        <label htmlFor="location" className="text-sm font-medium text-gray-900 block mb-2">Lokasi</label>
        <input ref={refLocationInput} type="text" id="location"
          className="bg-gray-50 border border-gray-300 text-gray-900 mb-3 sm:text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
          required></input>
        <button type="button" id="btn-location" className={`btn btn-sm btn-primary ${!showLocationBtn && 'hidden'} ${buttonLoading && 'loading'}`}
          onClick={getLocation}>Lokasi Sekarang</button>
      </div>
      <button type="submit" className="btn btn-lg btn-info mx-auto w-full">
        Simpan Note
      </button>
    </form>
  )
}

export default Post;