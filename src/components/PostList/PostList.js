import React from "react";
import Card from "../Card/Card";
import Skeleton from "../Skeleton/Skeleton";
import PropTypes from 'prop-types';

function PostList(props){
  const { isLoading, posts, change } = props;
  if (isLoading) return (<Skeleton />);
  if(posts.length < 1) {
    return (
      <div id="empty-post-wrapper">
        <h2 className="text-center text-black text-2xl mb-5">Make a to-do Note with TeamTodo!</h2>
      </div>
    )
  }
  return(
    <>
      {posts.map(post => {
        const { username, caption, location, id, done } = post;
        return <Card key={id} change={()=>change(id)} done={done} username={username}
          location={location} caption={caption} />
      })}
    </>
  )
}

PostList.propTypes = {
  isLoading: PropTypes.bool,
  posts: PropTypes.array
}

PostList.defaultProps = {
  isLoading: false,
  posts: []
}

export default PostList;