import React, { useState, useContext } from "react";
import { configurePushSub } from "./registerPush";
import { Context } from '../../App';

function Drawer(){
  const [showNotificationBtn, setShowNotificationBtn] = useState(true);
  const { installPrompt } = useContext(Context);
  const { deferredPrompt, savePrompt }  = installPrompt;

  const askForNotificationPermission = () => {
    Notification.requestPermission(function(result){
      console.log('User choice', result);
      if(result !== 'granted'){
        console.log('No notification permission granted');
      } else {
        setShowNotificationBtn(false);
        configurePushSub();
      }
    });
  }

  const installApp = async e => {
    if(!deferredPrompt) {
      return;
    }
    deferredPrompt.prompt();
    const { outcome } = await deferredPrompt.userChoice;

    console.log(`User response to the install prompt: ${outcome}`);
    savePrompt(null);
  }

  return (
    <>
      <li>
      {
        deferredPrompt && (
          <button type="button" className="btn btn-outline border-base-300 text-base-300 mr-5" onClick={installApp}>
            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
              <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M4 16v1a3 3 0 003 3h10a3 3 0 003-3v-1m-4-4l-4 4m0 0l-4-4m4 4V4" />
            </svg>
            &nbsp; Install the app
          </button>
        )
      }
      </li>
      <li>
        <button className={`btn enable-notification-buttons ${!showNotificationBtn && 'hidden'}`}
         onClick={askForNotificationPermission}>Notify Me!</button>
      </li>
    </>
  )
}

export default Drawer;