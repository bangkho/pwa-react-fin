import React from "react";
import PropTypes from 'prop-types';

function Card(props){
  const { location, username, caption, done, change } = props;
  return (
    <div className="card text-left shadow-2xl mb-8 bg-white ">
      <div className="card-body p-5 flex flex-row items-center">
        <input type="checkbox" checked={done} className="checkbox checkbox-lg mr-5 border-base-100" onChange={change}/>
        <div className="flex flex-col w-full">
          <p className="font-normal text-black"><span className="text-black font-bold">{username}</span>
            : {caption}
          </p>
          <div className="flex mt-2">
            <svg className="fill-current text-red-400 mr-2 h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
              <path fillRule="evenodd" d="M5.05 4.05a7 7 0 119.9 9.9L10 18.9l-4.95-4.95a7 7 0 010-9.9zM10 11a2 2 0 100-4 2 2 0 000 4z" clipRule="evenodd" />
            </svg>
          <p className="font-medium text-black">{location}</p>
          </div>
        </div>
        {done && <div className="badge mx-2 badge-secondary font-bold justify-self-end">
          SELESAI
        </div>}
      </div>
    </div>
  )
}

Card.propTypes = {
  username: PropTypes.string,
  location: PropTypes.string,
  caption: PropTypes.string
}

Card.defaultProps = {
  username: 'johndoe',
  location: 'Jakarta',
  caption: 'Enjoy your life'
}

export default Card;