const functions = require('firebase-functions');
const admin = require('firebase-admin');
const cors = require('cors');

admin.initializeApp(functions.config().firebase);

exports.addTodo = functions.https.onRequest((request, response) =>{
    cors({ origin: true })(request, response, () => {
      admin.database()
      .ref(`/todos/{${new Date().toISOString()}}`)
      .once(request.body)
      .then(snap => {
        console.log(snap);
        response.send({ code: 201, success: true, data: snap })
      })
      .catch(err => {
        console.log(err);
        response.send({ code: 400, success: false })
      })
    })
  })

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//   functions.logger.info("Hello logs!", {structuredData: true});
//   response.send("Hello from Firebase!");
// });
